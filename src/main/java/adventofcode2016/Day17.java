package adventofcode2016;

import org.testng.annotations.Test;

public class Day17 {
    /*
        #########
        #S| | | #
        #-#-#-#-#
        # | | | #
        #-#-#-#-#
        # | | | #
        #-#-#-#-#
        # | | |
        ####### V
     */

    int maxX = 3;
    int maxY = -3;


    enum KeyIdx {
        U(0), D(1), L(2), R(3);

        private final int idx;

        KeyIdx(int idx) {
            this.idx = idx;
        }

        int getIdx() {
            return this.idx;
        }
    }

    @Test
    public void testExample1() {
        String passCode = "ihgpwlah";
        String firstKey = md5(passCode);
        String substring = firstKey.substring(0, 4);

        for (int i = 0; i < 10; i++) {
            int[] path = findPath(substring);
        }
        System.out.println(substring);
        System.out.println(substring.charAt(KeyIdx.D.idx));

    }

    @Test
    public void real1() {
        String passCode = "pslxynzg";
        String firstKey = md5(passCode);
        String substring = firstKey.substring(0, 4);

        for (int i = 0; i < 10; i++) {
            int[] path = findPath(substring);
        }
        System.out.println(substring);
        System.out.println(substring.charAt(KeyIdx.D.idx));

    }

    private int[] findPath(String substring) {
        int[] dirs = new int[4];
        for (int i = 0; i < substring.length(); i++) {
            if (substring.charAt(i) == 'b' || substring.charAt(i) == 'c' ||
                    substring.charAt(i) == 'd' || substring.charAt(i) == 'e' || substring.charAt(i) == 'f') {
                dirs[i] = 1;
            }
        }
        return dirs;
    }

    private boolean isOpen(char c) {
        return c == 'b' || c == 'c' || c == 'd' || c == 'e' || c == 'f';
    }

    private static String md5(String input) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(input.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (byte anArray : array) {
                sb.append(String.format("%02x", anArray));
            }
            return sb.toString();
        } catch (Exception e) {
            return null;
        }
    }
}
