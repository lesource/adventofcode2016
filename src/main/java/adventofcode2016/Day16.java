package adventofcode2016;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class Day16 {

    @Test
    public void exampleP1() {
        int length = 20;
        String curr = "10000";

       runTest(length, curr);
    }

    @Test
    public void exampleP2() {
        int length = 20;
        String curr = "10000";

        runTest2(length, curr);
    }

    @Test
    public void realP1() {
        int length = 272;
        String curr = "01110110101001000";

        runTest(length, curr);
    }

    @Test
    public void realP2() {
        int length = 35651584;
        String curr = "01110110101001000";

        runTest2(length, curr);
    }

    private void runTest2(int length, String curr) {
        List<Character> thaList = new ArrayList<>();
        for (char c : curr.toCharArray()) {
            thaList.add(c);
        }

        while (thaList.size() < length) {
            dragonCurve2(thaList);
        }

        List<Character> newList = thaList.subList(0, length);

        List<Character> chsum = makeCheckSum2(newList);
        while (chsum.size() % 2 != 1) {
            chsum = makeCheckSum2(chsum);
        }

        for (Character c : chsum) {
            System.out.print(c);
        }
    }

    private List<Character> makeCheckSum2(List<Character> newList) {
        List<Character> chksum = new ArrayList<>();
        for (int i = 0; i < newList.size(); i+=2) {
            if (newList.get(i) == newList.get(i + 1)) {
                chksum.add('1');
            } else {
                chksum.add('0');
            }
        }
        return chksum;
    }

    private void dragonCurve2(List<Character> thaList) {
        List<Character> b = new ArrayList<>();
        for (int i = thaList.size() - 1; i >=0; i--) {
            if (thaList.get(i) == '0') {
                b.add('1');
            } else {
                b.add('0');
            }
        }
        thaList.add('0');
        thaList.addAll(b);
    }

    private void runTest(int length, String curr) {
        while (curr.length() < length) {
            curr = dragonCurve(curr);
        }
        String needed = curr.substring(0, length);

        String chsum = makeCheckSum(needed);
        while (chsum.length() % 2 != 1) {
            chsum = makeCheckSum(chsum);
        }

        System.out.println(chsum);
    }

    private String makeCheckSum(String needed) {
        String tmp = "";
        for (int i = 0; i < needed.length(); i+=2) {
            if (needed.charAt(i) == needed.charAt(i + 1)) {
                tmp += "1";
            } else {
                tmp += "0";
            }
        }
        return tmp;
    }

    private String dragonCurve(String curr) {
        String b = revChars(curr);
        return curr + "0" + b;
    }

    private String revChars(String b) {
        String tmp = "";
        for (int i = b.length() - 1; i >= 0; i--) {
            if (b.charAt(i) == '0') {
                tmp += "1";
            } else {
                tmp += "0";
            }
        }
        return tmp;
    }
}
