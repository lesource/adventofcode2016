package adventofcode2016;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day15 {

    private String realInput = "Disc #1 has 13 positions; at time=0, it is at position 1.\n" +
            "Disc #2 has 19 positions; at time=0, it is at position 10.\n" +
            "Disc #3 has 3 positions; at time=0, it is at position 2.\n" +
            "Disc #4 has 7 positions; at time=0, it is at position 1.\n" +
            "Disc #5 has 5 positions; at time=0, it is at position 3.\n" +
            "Disc #6 has 17 positions; at time=0, it is at position 5.\n" +
            "Disc #7 has 11 positions; at time=0, it is at position 0.";

    @Test
    public void realTestP1() {
        runTest(5, realInput);
    }

    @Test
    public void realTestP2() {
        runTest(6, realInput);
    }

    private void runTest(int numOfDisc, String input) {
        List<DiscInfo> discInfos = parseInput(input);

        int time = 0;
        while (true) {
            boolean pass = true;
            for (int i = 0; i <= numOfDisc; i++) {
                pass &= (discInfos.get(i).currPosition + (i + 1)) % discInfos.get(i).positions == 0;
                discInfos.get(i).currPosition = (discInfos.get(i).currPosition + 1) % discInfos.get(i).positions;
            }
            if (pass) {
                break;
            }
            time++;
        }
        System.out.println("Time: " + time);
    }

    private List<DiscInfo> parseInput(String input) {
        List<DiscInfo> tmp = new ArrayList<>();
        String[] split = input.split("\\n");

        for (String s : split) {
            String pattern = "Disc #([0-9]*) has ([0-9]*).*position ([0-9]*).*";
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(s);
            if (m.find()) {
                int disc = Integer.parseInt(m.group(1));
                int positions = Integer.parseInt(m.group(2));
                int time0Pos = Integer.parseInt(m.group(3));
                tmp.add(new DiscInfo(disc, positions, time0Pos));
            } else {
                System.out.println("NO MATCH");
            }
        }
        return tmp;
    }

    private static class DiscInfo {
        int disc;
        int positions;
        int currPosition;

        DiscInfo(int disc, int positions, int currPosition) {
            this.disc = disc;
            this.positions = positions;
            this.currPosition = currPosition;
        }
    }
}
