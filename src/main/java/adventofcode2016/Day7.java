package adventofcode2016;

import org.testng.annotations.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Day7 {

    @Test
    public void exampleTest() {
        String ok1 = "abba[mnop]qrst";
        String ok2 = "abcd[bddb]xyyx";
        String nok1 = "aaaa[qwer]tyui";
        String nok2 = "ioxxoj[asdfgh]zxcvbn";
        String realExample = "wysextplwqpvipxdv[srzvtwbfzqtspxnethm]syqbzgtboxxozpwr[kljvjjkjyojzrstfgrw]obdhcczonzvbfby[svotajtpttohxsh]cooktbyumlpxostt";
        String ex2 = "pyeiwajmvimttmi[pkvpbhnufrwpwdwy]dwdpuymdrskpgfwp";

        String[] input = new String[]{ok1, ok2, nok1, nok2, realExample};

        for (String s : input) {
            System.out.println(s);
            boolean res = parseLine(s);

            System.out.println(res);
        }
    }

    @Test
    public void exampleTestPart2() {
        String in = "aba[bab]xyz\n" +
                "xyx[xyx]xyx\n" +
                "aaa[kek]eke\n" +
                "zazbz[bzb]cdb\n";


        String[] input = in.split("\\n");

        for (String s : input) {
            System.out.println(s);
            boolean res = parseLine2(s);

            System.out.println(res);
        }
    }



    @Test
    public void realTestPart1() throws FileNotFoundException {
        File file = new File("C:\\Users\\Henrik\\IdeaProjects\\firstRun\\src\\main\\java\\Resources\\day7Input");
        Scanner input = new Scanner(file);

        List<String> realInput = new ArrayList<>();
        while (input.hasNext()) {
            realInput.add(input.nextLine());
        }

        input.close();

        int count = 0;
        for (String s : realInput) {
            System.out.println(s);
            boolean res = parseLine(s);
            if (res) {
                count++;
            }

            System.out.println(res);
        }
        System.out.println("Valid: " + count);
    }

    @Test
    public void realTestPart2() throws FileNotFoundException {
        File file = new File("C:\\Users\\Henrik\\IdeaProjects\\firstRun\\src\\main\\java\\Resources\\day7Input");
        Scanner input = new Scanner(file);

        List<String> realInput = new ArrayList<>();
        while (input.hasNext()) {
            realInput.add(input.nextLine());
        }

        input.close();

        int count = 0;
        for (String s : realInput) {
            System.out.println(s);
            boolean res = parseLine2(s);
            if (res) {
                count++;
            }

            System.out.println(res);
        }
        System.out.println("Valid: " + count);
    }

    @Test
    public void onlyOneCharacter() {
        checkSeq("aaaa");
    }

    private boolean parseLine(String s) {
        char[] chars = s.toCharArray();
        boolean outsideBrackets = false;
        boolean insideBrackets = true;

        String tmp = "";
        for (char c : chars) {
            if (c == '[') {
                boolean res = checkOkSequence(tmp);
                System.out.println("Seq: " + tmp + ", res: " + res);
                outsideBrackets |= res;
                tmp = "";
            } else if (c == ']') {
                boolean res = checkNOkSequence(tmp);
                System.out.println("NO seq: " + tmp + ", res: " + res);
                insideBrackets &= res;
                tmp = "";
            } else {
                tmp += c;
            }
        }
        boolean res = checkOkSequence(tmp);
        System.out.println("Seq: " + tmp + ", res: " + res);
        outsideBrackets |= res;

        return outsideBrackets & insideBrackets;
    }

    private boolean parseLine2(String s) {
        char[] chars = s.toCharArray();

        List<String> outsideSeqs = new ArrayList<>();
        List<String> insideSeqs = new ArrayList<>();

        String tmp = "";
        for (char c : chars) {
            if (c == '[') {
                outsideSeqs.addAll(getSequence(tmp));
                tmp = "";
            } else if (c == ']') {
                List<String> temp = new ArrayList<>(getSequence(tmp));
                for (String st : temp) {
                    insideSeqs.add("" + st.charAt(1) + st.charAt(0) + st.charAt(1));
                }
                tmp = "";
            } else {
                tmp += c;
            }
        }
        outsideSeqs.addAll(getSequence(tmp));
        System.out.println("Outside: " + outsideSeqs);
        System.out.println("Inside:" + insideSeqs);

        for (String a : outsideSeqs) {
            if (insideSeqs.contains(a)) {
                return true;
            }
        }
        return false;
    }

    private List<String> getSequence(String in) {
        char[] chars = in.toCharArray();
        List<String> tmp = new ArrayList<>();
        for (int i = 1; i < chars.length; i++) {
            if ((i + 1) >= chars.length) {
                break;
            }
            if (chars[i - 1] == chars[i + 1] && chars[i - 1] != chars[i]) {
                String t = "" + chars[i - 1] + chars[i] + chars[i + 1];
                tmp.add(t);
            }
        }
        return tmp;
    }

    private boolean checkNOkSequence(String subString) {
        return !checkSeq(subString);
    }

    private boolean checkOkSequence(String subString) {
        return checkSeq(subString);
    }

    private boolean checkSeq(String s) {
        char[] chars = s.toCharArray();

        for (int i = 2; i < chars.length; i++) {
            if ((i + 1) >= chars.length) {
                break;
            }
            if ((i + 1) <= chars.length && chars[i] == chars[i - 1] && chars[i - 2] == chars[i + 1] && chars[i] != chars[i + 1]) {
                return true;
            }
        }
        return false;
    }
}
