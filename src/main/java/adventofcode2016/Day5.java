package adventofcode2016;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class Day5 {

    @Test
    public void realTestPart1() {
        String data = "wtnhxymk";

        List<String> hashes = new ArrayList<>();
        for (int a = 0; a <= 30000000; a++) {
            String n = data + a;
            String hash = md5(n);
            if (hash == null) {
                break;
            }
            String startWithZeroes = hash.substring(0, 5);
            boolean good = startWithZeroes.equals("00000");

            if (good) {
                hashes.add(hash);
                System.out.println(hash);
                if (hashes.size() == 8) {
                    break;
                }
            }
        }

        String pin = "";
        for (String s : hashes) {
            pin += s.charAt(5);
            System.out.println("Hash: " + s); // 2414bc77
        }
        System.out.println(pin);
    }

    @Test
    public void realTestPart2() throws InterruptedException {
        String data = "wtnhxymk";
        char[] b = new char[] { '-', '-', '-', '-', '-', '-', '-', '-',};
        int numAdded = 1;
        List<String> hashes = new ArrayList<>();
        for (int a = 0; a <= 30000000; a++) {
            if (numAdded == 9) {
                break;
            }
            String n = data + a;
            String hash = md5(n);
            if (hash == null) {
                break;
            }
            String startWithZeroes = hash.substring(0, 5);
            boolean good = startWithZeroes.equals("00000");

            if (good) {
                int place = Character.getNumericValue(hash.charAt(5));
                if (place < 8 && b[place] == '-') {
                    b[place] = hash.charAt(6);
                    hashes.add(hash);
                    System.out.println("hash: "+ hash + ", current index: " + a);
                    numAdded++;
                }

            }
        }

        String pin = "";
        for (String s : hashes) {
            pin += s.charAt(5);
            System.out.println("Hash: " + s); // 2414bc77
        }
        System.out.println(pin);

        for (char c : b) {
            System.out.print(c);
        }
    }

    @Test
    public void printToSameLocation() throws InterruptedException {
        for (int i = 0; i < 60; i++) {
            System.out.println("Henrik");
            System.out.flush();
            Thread.sleep(1000);
        }
    }

    private static String md5(String input) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(input.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (byte anArray : array) {
                sb.append(String.format("%02x", anArray));
            }
            return sb.toString();
        } catch (Exception e) {
            return null;
        }
    }
}



