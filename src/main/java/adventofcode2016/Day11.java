package adventofcode2016;

import org.testng.annotations.Test;

import java.util.*;

public class Day11 {

    /*
    The first floor contains a strontium generator, a strontium-compatible microchip, a plutonium generator, and a plutonium-compatible microchip.
    The second floor contains a thulium generator, a ruthenium generator, a ruthenium-compatible microchip, a curium generator, and a curium-compatible microchip.
    The third floor contains a thulium-compatible microchip.
    The fourth floor contains nothing relevant.
     */

    /*
    F4 E  HG HM LG LM
    F3 .  .  .  .  .
    F2 .  .  .  .  .
    F1 .  .  .  .  .
     */

    /*
    The first floor contains a hydrogen-compatible microchip and a lithium-compatible microchip.
    The second floor contains a hydrogen generator.
    The third floor contains a lithium generator.
    The fourth floor contains nothing relevant.
     */

    int meFloor = 0;

    @Test
    public void exampleP1() {
        List<Integer> hl = new ArrayList<>();
        hl.add(0);
        hl.add(1);
        List<Integer> ll = new ArrayList<>();
        ll.add(0);
        ll.add(2);

        while (hl.get(0) != hl.get(1) && ll.get(0) != ll.get(1)) {

            if (hl.contains(meFloor)) {
                System.out.println(hl);
                if (hl.get(0) == hl.get(1)) {

                }
            }
        }

    }

    private void print(List<List<String>> floors) {
        for (int i = 0; i < floors.size(); i++) {
            System.out.println("Floor: "  + i + " " + floors.get(i));
        }
        System.out.println("Elevator: " + meFloor);
    }
}
