package adventofcode2016;

import org.testng.annotations.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day3 {

    @Test
    public void testInputPart1() throws FileNotFoundException {
        List<Triangle> triangleList = new ArrayList<>();
        triangleList.add(new Triangle(5, 10, 25));
        triangleList.add(new Triangle(15, 20, 25));

        int numOfTriangles = 0;
        for (Triangle t : triangleList) {
            if (t.isTriangle()) {
                numOfTriangles++;
            }
        }
        System.out.println("Number of triangles found: " + numOfTriangles);
    }

    @Test
    public void realTestPart1() throws FileNotFoundException {
        List<Triangle> triangleList = parseInputFilePart1();

        int numOfTriangles = 0;
        for (Triangle t : triangleList) {
            if (t.isTriangle()) {
                numOfTriangles++;
            }
        }
        System.out.println(String.format("Total items: %s, Triangles: %s", triangleList.size(), numOfTriangles));
    }

    @Test
    public void realTestPart2() throws FileNotFoundException {
        List<Triangle> triangleList = parseInputFilePart2();

        int numOfTriangles = 0;
        for (Triangle t : triangleList) {
            if (t.isTriangle()) {
                numOfTriangles++;
            }
        }
        System.out.println(String.format("Total items: %s, Triangles: %s", triangleList.size(), numOfTriangles));
    }

    private List<Triangle> parseInputFilePart1() throws FileNotFoundException {
        Scanner scanner = getScanner();
        List<Triangle> triangleList = new ArrayList<>();
        while(scanner.hasNextLine()){
            String line = scanner.nextLine();
            String pattern = "[ ]*([0-9]*)[ ]*([0-9]*)[ ]*([0-9]*)";
            // Create a Pattern object
            Pattern r = Pattern.compile(pattern);
            // Now create matcher object.
            Matcher m = r.matcher(line);
            if (m.find( )) {
                int a = Integer.parseInt(m.group(1));
                int b = Integer.parseInt(m.group(2));
                int c = Integer.parseInt(m.group(3));
                triangleList.add(new Triangle(a, b, c));
            }else {
                System.out.println("NO MATCH");
            }
        }
        scanner.close();
        return triangleList;
    }

    private List<Triangle> parseInputFilePart2() throws FileNotFoundException {
        Scanner scanner = getScanner();

        List<Integer> row1 = new ArrayList<>();
        List<Integer> row2 = new ArrayList<>();
        List<Integer> row3 = new ArrayList<>();

        while(scanner.hasNextLine()){
            String line = scanner.nextLine();
            String pattern = "[ ]*([0-9]*)[ ]*([0-9]*)[ ]*([0-9]*)";
            // Create a Pattern object
            Pattern r = Pattern.compile(pattern);
            // Now create matcher object.
            Matcher m = r.matcher(line);
            if (m.find( )) {
                row1.add(Integer.parseInt(m.group(1)));
                row2.add(Integer.parseInt(m.group(2)));
                row3.add(Integer.parseInt(m.group(3)));
            }else {
                System.out.println("NO MATCH");
            }
        }
        scanner.close();

        List<Triangle> triangleList = new ArrayList<>();
        fillTriangleList(triangleList, row1);
        fillTriangleList(triangleList, row2);
        fillTriangleList(triangleList, row3);
        return triangleList;
    }

    private void fillTriangleList(List<Triangle> triangleList, List<Integer> row) {
        for (int i = 0; i < row.size(); i += 3) {
            triangleList.add(new Triangle(row.get(i), row.get(i + 1), row.get(i + 2)));
        }
    }

    private Scanner getScanner() throws FileNotFoundException {
        //Scanner Example - read file line by line in Java using Scanner
        FileInputStream fis = new FileInputStream("C:\\Users\\Henrik\\IdeaProjects\\firstRun\\src\\main\\java\\Resources\\day3Input");
        return new Scanner(fis);
    }

    private static class Triangle {
        int a;
        int b;
        int c;

        Triangle(int a, int b, int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        boolean isTriangle() {
            boolean res = true;
            res &= (a + b) > c;
            res &= (b + c) > a;
            res &= (a + c) > b;
            return res;
        }
    }
}
