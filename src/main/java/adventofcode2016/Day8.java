package adventofcode2016;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Arrays;

public class Day8 {

    char[][] realBoard = new char[6][50];
    char[][] testBoard = new char[3][7];


    @BeforeClass
    public void setup() {
//        for (int i = 0; i < realBoard.length; i++) {
//            for (int j = 0; j < realBoard[0].length; j++) {
//                realBoard[i][j] = '.';
//            }
//        }
//
//        System.out.println("Start board");
//        for (int i = 0; i < realBoard.length; i++) {
//            for (int j = 0; j < realBoard[0].length; j++) {
//                System.out.print(realBoard[i][j]);
//            }
//            System.out.println("");
//        }
    }

    @Test
    public void testExample() {
        for (int i = 0; i < testBoard.length; i++) {
            for (int j = 0; j < testBoard[0].length; j++) {
                testBoard[i][j] = '.';
            }
        }

        printBoard(testBoard);
        String input = "rect 3x2\n" +
                "rotate column x=1 by 1\n" +
                "rotate row y=0 by 4\n" +
                "rotate column x=1 by 1\n";

        String[] split = input.split("\\n");

        for (String cmd : split) {
            String[] commands = getCommand(cmd);
            System.out.println(Arrays.toString(commands));
            printCommand(commands);
            printBoard(testBoard);
        }

    }

    @Test
    public void realTestPart1() {
        for (int i = 0; i < realBoard.length; i++) {
            for (int j = 0; j < realBoard[0].length; j++) {
                realBoard[i][j] = '.';
            }
        }
        printBoard(realBoard);

        String[] split = realInput.split("\\n");

        for (String cmd : split) {
            String[] commands = getCommand(cmd);
            System.out.println(Arrays.toString(commands));
            printCommand(commands);
            printBoard(realBoard);
        }

    }

    private char[][] initBoard(int x, int y) {
        char[][] tmp = new char[x][y];
        for (int i = 0; i < tmp.length; i++) {
            for (int j = 0; j < tmp[0].length; j++) {
                tmp[i][j] = '.';
            }
        }
        return tmp;
    }

    private void printBoard(char[][] board) {
        int count = 0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (board[i][j] == 'x') {
                    count++;
                }
                System.out.print(board[i][j]);
            }
            System.out.println("");
        }
        System.out.println("Number: " + count);
        System.out.println("");
    }

    private void printCommand(String[] commands) {
        String instruction = commands[0];
        char[][] tmpBoard = initBoard(realBoard.length, realBoard[0].length);
        if (instruction.equals("rect")) {
            int x = Integer.parseInt(commands[1]);
            int y = Integer.parseInt(commands[2]);

            for (int i = 0; i < tmpBoard.length; i++) {
                for (int j = 0; j < tmpBoard[0].length; j++) {
                    tmpBoard[i][j] = realBoard[i][j];
                }
            }
            for (int i = 0; i < y; i++) {
                for (int j = 0; j < x; j++) {
                    tmpBoard[i][j] = 'x';
                }
            }

        }
        else if (instruction.equals("rotate")) {
            String dir = commands[1];
            int rowColNum = Integer.parseInt(commands[2]);
            int steps = Integer.parseInt(commands[3]);

            if (dir.equals("x")) {
                for (int i = 0; i < tmpBoard.length; i++) {
                    for (int j = 0; j < tmpBoard[0].length; j++) {
                        if (j == rowColNum) {
                            int place = (i + steps) > (tmpBoard.length - 1) ? (i + steps) % tmpBoard.length : (i + steps);
                            tmpBoard[place][j] = realBoard[i][j];
                        } else {
                            tmpBoard[i][j] = realBoard[i][j];
                        }
//                        printBoard(tmpBoard);
                    }
//                    printBoard(tmpBoard);
                }

            }
            if (dir.equals("y")) {
                for (int i = 0; i < tmpBoard.length; i++) {
                    for (int j = 0; j < tmpBoard[0].length; j++) {
                        if (i == rowColNum) {
                            int place = (j + steps) > (tmpBoard[0].length -1) ? (j + steps) % (tmpBoard[0].length) : (j + steps);
                            tmpBoard[i][place] = realBoard[i][j];
                        } else {
                            tmpBoard[i][j] = realBoard[i][j];
                        }
//                        printBoard(tmpBoard);
                    }
//                    printBoard(tmpBoard);
                }
            }
        }
        realBoard = tmpBoard.clone();
    }

    private String[] getCommand(String cmd) {
        String[] subCmd = cmd.split(" ");
        if (subCmd[0].equals("rect")) {
            String[] coords = subCmd[1].split("x");
            return new String[] { subCmd[0], coords[0], coords[1] };
        }
        if (subCmd[0].equals("rotate")) {
            String[] colRowAndPlace = subCmd[2].split("=");
            return new String[] { subCmd[0], colRowAndPlace[0], colRowAndPlace[1], subCmd[4] };
        }
        return null;
    }

    String realInput = "rect 1x1\n" +
            "rotate row y=0 by 6\n" +
            "rect 1x1\n" +
            "rotate row y=0 by 3\n" +
            "rect 1x1\n" +
            "rotate row y=0 by 5\n" +
            "rect 1x1\n" +
            "rotate row y=0 by 4\n" +
            "rect 2x1\n" +
            "rotate row y=0 by 5\n" +
            "rect 2x1\n" +
            "rotate row y=0 by 2\n" +
            "rect 1x1\n" +
            "rotate row y=0 by 5\n" +
            "rect 4x1\n" +
            "rotate row y=0 by 2\n" +
            "rect 1x1\n" +
            "rotate row y=0 by 3\n" +
            "rect 1x1\n" +
            "rotate row y=0 by 3\n" +
            "rect 1x1\n" +
            "rotate row y=0 by 2\n" +
            "rect 1x1\n" +
            "rotate row y=0 by 6\n" +
            "rect 4x1\n" +
            "rotate row y=0 by 4\n" +
            "rotate column x=0 by 1\n" +
            "rect 3x1\n" +
            "rotate row y=0 by 6\n" +
            "rotate column x=0 by 1\n" +
            "rect 4x1\n" +
            "rotate column x=10 by 1\n" +
            "rotate row y=2 by 16\n" +
            "rotate row y=0 by 8\n" +
            "rotate column x=5 by 1\n" +
            "rotate column x=0 by 1\n" +
            "rect 7x1\n" +
            "rotate column x=37 by 1\n" +
            "rotate column x=21 by 2\n" +
            "rotate column x=15 by 1\n" +
            "rotate column x=11 by 2\n" +
            "rotate row y=2 by 39\n" +
            "rotate row y=0 by 36\n" +
            "rotate column x=33 by 2\n" +
            "rotate column x=32 by 1\n" +
            "rotate column x=28 by 2\n" +
            "rotate column x=27 by 1\n" +
            "rotate column x=25 by 1\n" +
            "rotate column x=22 by 1\n" +
            "rotate column x=21 by 2\n" +
            "rotate column x=20 by 3\n" +
            "rotate column x=18 by 1\n" +
            "rotate column x=15 by 2\n" +
            "rotate column x=12 by 1\n" +
            "rotate column x=10 by 1\n" +
            "rotate column x=6 by 2\n" +
            "rotate column x=5 by 1\n" +
            "rotate column x=2 by 1\n" +
            "rotate column x=0 by 1\n" +
            "rect 35x1\n" +
            "rotate column x=45 by 1\n" +
            "rotate row y=1 by 28\n" +
            "rotate column x=38 by 2\n" +
            "rotate column x=33 by 1\n" +
            "rotate column x=28 by 1\n" +
            "rotate column x=23 by 1\n" +
            "rotate column x=18 by 1\n" +
            "rotate column x=13 by 2\n" +
            "rotate column x=8 by 1\n" +
            "rotate column x=3 by 1\n" +
            "rotate row y=3 by 2\n" +
            "rotate row y=2 by 2\n" +
            "rotate row y=1 by 5\n" +
            "rotate row y=0 by 1\n" +
            "rect 1x5\n" +
            "rotate column x=43 by 1\n" +
            "rotate column x=31 by 1\n" +
            "rotate row y=4 by 35\n" +
            "rotate row y=3 by 20\n" +
            "rotate row y=1 by 27\n" +
            "rotate row y=0 by 20\n" +
            "rotate column x=17 by 1\n" +
            "rotate column x=15 by 1\n" +
            "rotate column x=12 by 1\n" +
            "rotate column x=11 by 2\n" +
            "rotate column x=10 by 1\n" +
            "rotate column x=8 by 1\n" +
            "rotate column x=7 by 1\n" +
            "rotate column x=5 by 1\n" +
            "rotate column x=3 by 2\n" +
            "rotate column x=2 by 1\n" +
            "rotate column x=0 by 1\n" +
            "rect 19x1\n" +
            "rotate column x=20 by 3\n" +
            "rotate column x=14 by 1\n" +
            "rotate column x=9 by 1\n" +
            "rotate row y=4 by 15\n" +
            "rotate row y=3 by 13\n" +
            "rotate row y=2 by 15\n" +
            "rotate row y=1 by 18\n" +
            "rotate row y=0 by 15\n" +
            "rotate column x=13 by 1\n" +
            "rotate column x=12 by 1\n" +
            "rotate column x=11 by 3\n" +
            "rotate column x=10 by 1\n" +
            "rotate column x=8 by 1\n" +
            "rotate column x=7 by 1\n" +
            "rotate column x=6 by 1\n" +
            "rotate column x=5 by 1\n" +
            "rotate column x=3 by 2\n" +
            "rotate column x=2 by 1\n" +
            "rotate column x=1 by 1\n" +
            "rotate column x=0 by 1\n" +
            "rect 14x1\n" +
            "rotate row y=3 by 47\n" +
            "rotate column x=19 by 3\n" +
            "rotate column x=9 by 3\n" +
            "rotate column x=4 by 3\n" +
            "rotate row y=5 by 5\n" +
            "rotate row y=4 by 5\n" +
            "rotate row y=3 by 8\n" +
            "rotate row y=1 by 5\n" +
            "rotate column x=3 by 2\n" +
            "rotate column x=2 by 3\n" +
            "rotate column x=1 by 2\n" +
            "rotate column x=0 by 2\n" +
            "rect 4x2\n" +
            "rotate column x=35 by 5\n" +
            "rotate column x=20 by 3\n" +
            "rotate column x=10 by 5\n" +
            "rotate column x=3 by 2\n" +
            "rotate row y=5 by 20\n" +
            "rotate row y=3 by 30\n" +
            "rotate row y=2 by 45\n" +
            "rotate row y=1 by 30\n" +
            "rotate column x=48 by 5\n" +
            "rotate column x=47 by 5\n" +
            "rotate column x=46 by 3\n" +
            "rotate column x=45 by 4\n" +
            "rotate column x=43 by 5\n" +
            "rotate column x=42 by 5\n" +
            "rotate column x=41 by 5\n" +
            "rotate column x=38 by 1\n" +
            "rotate column x=37 by 5\n" +
            "rotate column x=36 by 5\n" +
            "rotate column x=35 by 1\n" +
            "rotate column x=33 by 1\n" +
            "rotate column x=32 by 5\n" +
            "rotate column x=31 by 5\n" +
            "rotate column x=28 by 5\n" +
            "rotate column x=27 by 5\n" +
            "rotate column x=26 by 5\n" +
            "rotate column x=17 by 5\n" +
            "rotate column x=16 by 5\n" +
            "rotate column x=15 by 4\n" +
            "rotate column x=13 by 1\n" +
            "rotate column x=12 by 5\n" +
            "rotate column x=11 by 5\n" +
            "rotate column x=10 by 1\n" +
            "rotate column x=8 by 1\n" +
            "rotate column x=2 by 5\n" +
            "rotate column x=1 by 5";
}
