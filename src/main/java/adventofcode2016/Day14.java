package adventofcode2016;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class Day14 {

    @Test
    public void testExampleP1() {
        String input = "abc";
        runTestP1(input);
    }

    @Test
    public void realP1() {
        String input = "ihaygndm";
        runTestP1(input);
    }

    @Test
    public void testP2() {
        String input = "abc";
        runTestP2(input);
    }

    @Test
    public void realP2() {
        String input = "ihaygndm";
        runTestP2(input);
    }

    private void runTestP1(String input) {
        int idx = 0;
        List<String> keys = new ArrayList<>();
        while (keys.size() != 64) {
            String threeHash = md5(input + idx);
            char threeSequence = isThreeSequence(threeHash);
            if (threeSequence != '-') {
                for (int i = (idx + 1); i < (idx + 1 + 1000); i++) {
                    String fiveHash = md5(input + i);
                    boolean key = fiveSequence(fiveHash, threeSequence);
                    if (key) {
                        keys.add(threeHash);
                        System.out.println(String.format("idx: %s, key: %s", idx, threeHash));
                    }
                }
            }
            if (keys.size() == 64) {
                break;
            }
            idx++;
        }

        for (int i = 0; i < keys.size(); i++) {
            System.out.println("Key: " + i + ", " + keys.get(i));
        }
        System.out.println("Idx: " + idx);
    }
    private void runTestP2(String input) {
        int idx = 0;
        List<String> keys = new ArrayList<>();
        while (keys.size() != 64) {
            String threeHash = md5(input + idx);
            for (int i = 0; i < 2016; i++) {
                threeHash = md5(threeHash);
            }

            char threeSequence = isThreeSequence(threeHash);
            if (threeSequence != '-') {
                for (int i = (idx + 1); i < (idx + 1 + 1000); i++) {
                    String fiveHash = md5(input + i);
                    boolean key = fiveSequence(fiveHash, threeSequence);
                    if (key) {
                        keys.add(threeHash);
                        System.out.println(String.format("idx: %s, key: %s", idx, threeHash));
                        System.out.println(String.format("idx: %s, key: %s", idx, fiveHash));
                    }
                }
            }
            if (keys.size() == 64) {
                break;
            }
            idx++;
        }

        for (int i = 0; i < keys.size(); i++) {
            System.out.println("Key: " + i + ", " + keys.get(i));
        }
        System.out.println("Idx: " + idx);
    }

    private boolean fiveSequence(String hash, char threeSequence) {
        char[] chars = hash.toCharArray();
        for (int i = 2; i < chars.length; i++) {
            if ((i + 2) < chars.length &&
                    chars[i - 2] == chars[i] &&
                    chars[i - 1] == chars[i] &&
                    chars[i] == chars[i + 1] &&
                    chars[i - 2] == chars[i + 2] &&
                    chars[i] == threeSequence) {
                return true;
            }
        }
        return false;
    }

    private char isThreeSequence(String hash) {
        char[] chars = hash.toCharArray();
        for (int i = 1; i < chars.length; i++) {
            if ((i + 1) < chars.length && chars[i - 1] == chars[i] && chars[i] == chars[i + 1]) {
                return chars[i];
            }
        }
        return '-';
    }

    private static String md5(String input) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(input.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (byte anArray : array) {
                sb.append(String.format("%02x", anArray));
            }
            return sb.toString();
        } catch (Exception e) {
            return null;
        }
    }
}
