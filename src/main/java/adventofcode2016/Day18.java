package adventofcode2016;

import org.testng.annotations.Test;

public class Day18 {

    @Test
    public void testExample() {
        char[] currRow = ".^^.^.^^^^".toCharArray();
        runTest(currRow, 10, true);
    }

    @Test
    public void realP1() {
        char[] currRow = "^..^^.^^^..^^.^...^^^^^....^.^..^^^.^.^.^^...^.^.^.^.^^.....^.^^.^.^.^.^.^.^^..^^^^^...^.....^....^.".toCharArray();
        runTest(currRow, 40, true);
    }

    @Test
    public void realP2() {
        char[] currRow = "^..^^.^^^..^^.^...^^^^^....^.^..^^^.^.^.^^...^.^.^.^.^^.....^.^^.^.^.^.^.^.^^..^^^^^...^.....^....^.".toCharArray();
        runTest(currRow, 400000, false);
    }

    private void runTest(char[] currRow, int numOfRows, boolean print) {
        char[][] rows = new char[numOfRows][currRow.length];
        rows[0] = currRow;
        for (int i = 1; i < numOfRows; i++) {
            currRow = getNextRow(currRow);
            rows[i] = currRow;
        }

        int count = 0;
        for (int i = 0; i < numOfRows; i++) {
            for (int j = 0; j < currRow.length; j++) {
                if (rows[i][j] == '.') {
                    count++;
                }
                if (print) {
                    System.out.print(rows[i][j]);
                }
            }
            if (print) {
                System.out.println("");
            }
        }

        System.out.println("Num of safe tiles" + count);
    }

    private char[] getNextRow(char[] currRow) {
        char[] newRow = new char[currRow.length];
        newRow[0] = getNextChar('.', currRow[0], currRow[1]);
        for (int i = 1; i <= currRow.length - 2; i++) {
            newRow[i] = getNextChar(currRow[i - 1], currRow[i], currRow[i + 1]);
        }
        newRow[currRow.length - 1] = getNextChar(currRow[currRow.length - 2], currRow[currRow.length - 1], '.');

        return newRow;
    }

    private char getNextChar(char c, char c1, char c2) {
        /*
            A new tile is a trap only in one of the following situations:
            Its left and center tiles are traps, but its right tile is not.
            Its center and right tiles are traps, but its left tile is not.
            Only its left tile is a trap.
            Only its right tile is a trap.
         */
        if ((c == '^' && c1 == '^' && c2 == '.') ||
                (c == '.' && c1 == '^' && c2 == '^') ||
                (c == '^' && c1 == '.' && c2 == '.') ||
                (c == '.' && c1 == '.' && c2 == '^')) {
            return '^';
        }
        return '.';
    }
}
