package adventofcode2016;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Day12 {

    String exInput = "cpy 41 c\n" +
            "cpy c a\n" +
            "inc a\n" +
            "inc a\n" +
            "dec a\n" +
            "jnz a 2\n" +
            "dec a";

    String realInput = "cpy 1 a\n" +
            "cpy 1 b\n" +
            "cpy 26 d\n" +
            "jnz c 2\n" +
            "jnz 1 5\n" +
            "cpy 7 c\n" +
            "inc d\n" +
            "dec c\n" +
            "jnz c -2\n" +
            "cpy a c\n" +
            "inc a\n" +
            "dec b\n" +
            "jnz b -2\n" +
            "cpy c b\n" +
            "dec d\n" +
            "jnz d -6\n" +
            "cpy 16 c\n" +
            "cpy 17 d\n" +
            "inc a\n" +
            "dec d\n" +
            "jnz d -2\n" +
            "dec c\n" +
            "jnz c -5";

    private Map<String, Integer> regIdx = new HashMap<>();
    private int[] regs = new int[]{ 0, 0, 1, 0 };

    @BeforeClass
    public void setup() {
        regIdx.put("a", 0);
        regIdx.put("b", 1);
        regIdx.put("c", 2);
        regIdx.put("d", 3);
    }

    @Test
    public void testExample() {
        String[] instructions = exInput.split("\\n");
        execute(instructions);
    }

    @Test
    public void realTest() {
        String[] instructions = realInput.split("\\n");
        execute(instructions);
    }

    private void execute(String[] instructions) {
        for (int i = 0; i < instructions.length; i++) {
            String[] inst = instructions[i].split(" ");
            if (inst[0].equals("cpy")) {
                int value;
                if (regIdx.containsKey(inst[1])) {
                    value = regs[regIdx.get(inst[1])];
                } else {
                    value = Integer.parseInt(inst[1]);
                }
                regs[regIdx.get(inst[2])] = value;
            }
            if (inst[0].equals("inc")) {
                regs[regIdx.get(inst[1])] += 1;
            }
            if (inst[0].equals("dec")) {
                regs[regIdx.get(inst[1])] -= 1;
            }
            if (inst[0].equals("jnz")) {
                if (regIdx.containsKey(inst[1])) {
                    if (regs[regIdx.get(inst[1])] != 0) {
                        i += Integer.parseInt(inst[2]) - 1;
                    }
                } else {
                    if (Integer.parseInt(inst[1]) != 0) {
                        i += Integer.parseInt(inst[2]) - 1;
                    }
                }
            }
        }
        System.out.println(Arrays.toString(regs));
    }
}
