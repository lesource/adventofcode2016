package adventofcode2016;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day4 {

    private String[] input = new String[] {
            "aaaaa-bbb-z-y-x-123[abxyz]",
            "a-b-c-d-e-f-g-h-987[abcde]",
            "not-a-real-room-404[oarel]",
            "totally-real-room-200[decoy]"};

    private List<Character> alphaList = new ArrayList<>();

    @BeforeClass
    public void setup() {
        char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        for (char c : alphabet) {
            alphaList.add(c);
        }
    }

    @Test
    public void testExamplePart1() {
        int num = 0;
        for (String line : input) {
            RoomCodeHolder roomCodeHolder = parseLine(line);
            System.out.println(roomCodeHolder);
            num += roomCodeHolder.runDecode();
        }
        System.out.println("Total room id: " + num);
    }

    @Test
    public void realTestPart1() throws FileNotFoundException {
        List<String> input = parseInputFilePart1();
        int num = 0;
        for (String line : input) {
            RoomCodeHolder roomCodeHolder = parseLine(line);
            System.out.println(roomCodeHolder);
            num += roomCodeHolder.runDecode();
        }
        System.out.println("Total room id: " + num);
    }

    @Test
    public void testExamplePart2() {
        String first = "qzmt-zixmtkozy-ivhz-343[abxyz]";


        RoomCodeHolder rch = parseLine(first);
        for (char c : rch.encName.toCharArray()) {
            if (c == '-') {
                System.out.print(" ");
                continue;
            }
            int charLoc = alphaList.indexOf(c);
            int shift = rch.sectorId % alphaList.size();

            System.out.print(alphaList.get((shift + charLoc) % alphaList.size()));
        }
    }

    @Test
    public void realTestPart2() throws FileNotFoundException {
        List<String> input = parseInputFilePart1();
        String toFind = "northpoleobject";
        for (String line : input) {
            RoomCodeHolder rch = parseLine(line);
            String decoded = "";
            for (char c : rch.encName.toCharArray()) {
                if (c == '-') {
                    System.out.print(" ");
                    continue;
                }
                int charLoc = alphaList.indexOf(c);
                int shift = rch.sectorId % alphaList.size();

                decoded += alphaList.get((shift + charLoc) % alphaList.size());
            }
            if (decoded.contains(toFind)) {
                System.out.println("Decoded: " + decoded);
                System.out.println(rch.toString());
            }
        }
    }

    private List<String> parseInputFilePart1() throws FileNotFoundException {
        Scanner scanner = getScanner();
        List<String> input = new ArrayList<>();
        while(scanner.hasNextLine()){
            String line = scanner.nextLine();
            input.add(line);
        }
        scanner.close();
        return input;
    }

    private RoomCodeHolder parseLine(String line) {
        String pattern = "([a-z\\-]*)([0-9]*)\\[(.*)]";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(line.toLowerCase());
        RoomCodeHolder rch = null;
        if (m.find( )) {
            String encName = m.group(1);
            int sectorId = Integer.parseInt(m.group(2));
            String checksum = m.group(3);
            rch = new RoomCodeHolder(encName, sectorId, checksum);
        }else {
            System.out.println("NO MATCH");
        }
        return rch;
    }

    private Scanner getScanner() throws FileNotFoundException {
        //Scanner Example - read file line by line in Java using Scanner
        FileInputStream fis = new FileInputStream("C:\\Users\\Henrik\\IdeaProjects\\firstRun\\src\\main\\java\\Resources\\day4Input");
        return new Scanner(fis);
    }

    private static class RoomCodeHolder {
        String encName;
        int sectorId;
        String checksum;
        List<Character> charList = new ArrayList<>();

        RoomCodeHolder(String encName, int sectorId, String checksum) {
            this.encName = encName.replaceAll("-", "");
            this.sectorId = sectorId;
            this.checksum = checksum;
        }

        int runDecode() {
            findAndSort();
            return isRealRoom() ? sectorId : 0;
        }

        private boolean isRealRoom() {
            char[] checksumArray = checksum.toCharArray();
            for (int i = 0; i < 5; i++) {
                if (checksumArray[i] != charList.get(i)) {
                    return false;
                }
            }
            return true;
        }

        void findAndSort() {
            char[] chars = encName.toCharArray();
            Map<Character, Integer> charMap = new TreeMap<>();
            Set<Character> charSet = new LinkedHashSet<>();
            for (char c : chars) {
                charSet.add(c);
            }

            for (Character c : charSet) {
                int num = count(chars, c);
                charMap.put(c, num);
            }

            // 1. Convert Map to List of Map
            List<Map.Entry<Character, Integer>> list = new LinkedList<>(charMap.entrySet());

            // 2. Sort list with Collections.sort(), provide a custom Comparator
            //    Try switch the o1 o2 position for a different order
            list.sort((o1, o2) -> (o2.getValue()).compareTo(o1.getValue()));

            // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap

            for (Map.Entry<Character, Integer> entry : list) {
                charList.add(entry.getKey());
            }
        }

        private int count(char[] chars, char cTo) {
            int num = 0;
            for (char c : chars) {
                if (c == cTo) {
                    num++;
                }
            }
            return num;
        }

        @Override
        public String toString() {
            return "RoomCodeHolder{" +
                    "encName='" + encName + '\'' +
                    ", sectorId=" + sectorId +
                    ", checksum='" + checksum + '\'' +
                    '}';
        }
    }
}
