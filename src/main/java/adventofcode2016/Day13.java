package adventofcode2016;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Day13 {

    private static final int REAL_INPUT = 1352;
    private static final int TEST_INPUT = 1352;
    private int[] startP = new int[]{ 1, 1 };
    List<List<Character>> board = new ArrayList<>();
    char[][] map = new char[50][50];

    @Test
    public void exampleP1() {
        int[] targetP = new int[]{ 7, 4 };
        boolean run = true;
        int[] currP = startP.clone();
        initBoard();
//        printBoard();

        printAll();
        printBoard();


//        while(run) {
//            if (Arrays.equals(currP, targetP)) {
//                run = false;
//            }
//            currP = takeStep(currP, targetP);
//            printBoard();
//        }
    }

    private void printAll() {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                if (j == 39 && i == 31) {
                    map[j][i] = 'X';
                    continue;
                }
                if (isOpenSpace(i, j)) {
                    map[j][i] = '.';
                } else {
                    map[j][i] = '#';
                }
            }
        }
    }

    private void initBoard() {
       for (int i = 0; i < map.length; i++) {
           for (int j = 0; j < map[0].length; j++) {
               map[i][j] = '@';
           }
       }
    }

    private void printBoard() {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                System.out.print(map[i][j]);
            }
            System.out.println("");
        }
    }

    private int[] takeStep(int[] currP, int[] targetP) {
        int[] newDir = new int[]{0, 0 };
        if (currP[0] < targetP[0] && currP[1] < targetP[1]) {
            int xDiff = Math.abs(currP[0] - targetP[0]);
            int yDiff = Math.abs(currP[1] - targetP[1]);
            if (xDiff > yDiff) {
                newDir = new int[]{ currP[0] + 1, currP[1] + 0 };
            } else {
                newDir = new int[]{ currP[0] + 0, currP[1] + 1 };
            }
        }

        boolean openSpace = isOpenSpace(newDir[0], newDir[1]);
        if (openSpace) {
            updateBoard(newDir, true);
        } else {
            updateBoard(newDir, false);
        }
        return newDir.clone();
    }

    private void updateBoard(int[] newPoint, boolean openSpace) {
        if (map[newPoint[0]][newPoint[1]] == '@') {
            if (openSpace) {
                map[newPoint[0]][newPoint[1]] = '.';
            } else {
                map[newPoint[0]][newPoint[1]] = '#';
            }
        }
    }

    private boolean isOpenSpace(int x, int y) {
        /*
            Find x*x + 3*x + 2*x*y + y + y*y.
            Add the office designer's favorite number (your puzzle input).
            Find the binary representation of that sum; count the number of bits that are 1.
            If the number of bits that are 1 is even, it's an open space.
            If the number of bits that are 1 is odd, it's a wall.
         */
        int res = (x * x) + (3 * x) + (2 * x * y) + y + (y * y) + TEST_INPUT;
        String binValue = Integer.toBinaryString(res);
        int countOnes = 0;
        for (char c : binValue.toCharArray()) {
            if (c == '1') {
                countOnes++;
            }
        }
        System.out.println("ones: " + countOnes);
        System.out.println("even / odd: " + countOnes % 2);
        return countOnes % 2 == 0;
    }
}
