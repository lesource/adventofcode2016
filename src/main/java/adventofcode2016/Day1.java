package adventofcode2016;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Day1 {

    enum Dir {
        N, E, W, S
    }

    public static void main(String[] args) {
        String input = "R1, R1, R3, R1, R1, L2, R5, L2, R5, R1, R4, L2, R3, L3, R4, L5, R4, R4, R1, L5, L4, R5, R3, L1, R4, " +
                "R3, L2, L1, R3, L4, R3, L2, R5, R190, R3, R5, L5, L1, R54, L3, L4, L1, R4, R1, R3, L1, L1, R2, L2, R2, R5, " +
                "L3, R4, R76, L3, R4, R191, R5, R5, L5, L4, L5, L3, R1, R3, R2, L2, L2, L4, L5, L4, R5, R4, R4, R2, R3, R4, " +
                "L3, L2, R5, R3, L2, L1, R2, L3, R2, L1, L1, R1, L3, R5, L5, L1, L2, R5, R3, L3, R3, R5, R2, R5, R5, L5, L5, " +
                "R2, L3, L5, L2, L1, R2, R2, L2, R2, L3, L2, R3, L5, R4, L4, L5, R3, L4, R1, R3, R2, R4, L2, L3, R2, L5, R5, " +
                "R4, L2, R4, L1, L3, L1, L3, R1, R2, R1, L5, R5, R3, L3, L3, L2, R4, R2, L5, L1, L1, L5, L4, L1, L1, R1";

        Point p = new Point(0, 0);
        Point pInit = new Point(0, 0);
        List<Point> points = new ArrayList<>();
        Dir dir = Dir.N;
        List<String> inputList = Arrays.asList(input.split("\\s*,\\s*"));

        for (String in : inputList) {
            char direction = in.charAt(0);
            int length = Integer.parseInt(in.substring(1, in.length()));
            dir = newDir(dir, direction);
            int[] newDir = newDir(dir);

            for (int i = 0; i < length; i++) {
                Point newPoint = new Point(p.x + (i * newDir[0]), p.y + (i * newDir[1]));
                points.add(newPoint);
            }

            p.x += length * newDir[0];
            p.y += length * newDir[1];

            System.out.println(String.format("%d, %d", p.x, p.y));
        }

        int dist = Math.abs(pInit.x - p.x) + Math.abs(pInit.y - p.y);

        Point stopPoint = new Point(0 ,0);
        boolean stop = true;
        List<Point> tmpPoint = new ArrayList<>(points);
        for (int i = 0; i < tmpPoint.size(); i++) {
            Point iPoint = tmpPoint.get(0);
            for (int j = 1; j < tmpPoint.size(); j++) {
                Point jPoint = tmpPoint.get(j);
                if (iPoint.x == jPoint.x && iPoint.y == jPoint.y && stop) {
                    System.out.println(points.get(i));
                    stopPoint.x = iPoint.x;
                    stopPoint.y = iPoint.y;
                    stop = false;
                }
            }
            tmpPoint.remove(0);
        }

        System.out.println("Part1: Distance=" + dist);
        System.out.println("Part2: Point visited twice=" + stopPoint);
        System.out.println("Part2: Distance to visited twice=" + (Math.abs(pInit.x - stopPoint.x) + Math.abs(pInit.y - stopPoint.y)));
    }

    private static Dir newDir(Dir dir, char direction) {
        switch (dir) {
            case N:
                return direction == 'R' ? Dir.E : Dir.W;
            case E:
                return direction == 'R' ? Dir.S : Dir.N;
            case W:
                return direction == 'R' ? Dir.N : Dir.S;
            case S:
                return direction == 'R' ? Dir.W : Dir.E;
        }
        return null;
    }

    private static int[] newDir(Dir dir) {
        switch (dir) {
            case N:
                return new int[] { 0, 1 };
            case E:
                return new int[] { 1, 0 };
            case W:
                return new int[] { -1, 0 };
            case S:
                return new int[] { 0, -1 };
        }
        return new int[] { 0, 0 };
    }

    private static class Point {
        int x;
        int y;

        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return String.format("[%d, %d]", x, y);
        }
    }
}
