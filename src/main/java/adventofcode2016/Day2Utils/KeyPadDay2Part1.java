package adventofcode2016.Day2Utils;

public class KeyPadDay2Part1 extends KeyPad {

    public KeyPadDay2Part1() {
        int[] row0 = new int[] { 0, 0, 0, 0, 0 };
        int[] row1 = new int[] { 0, 1, 2, 3, 0 };
        int[] row2 = new int[] { 0, 4, 5, 6, 0 };
        int[] row3 = new int[] { 0, 7, 8, 9, 0 };
        int[] row4 = new int[] { 0, 0, 0, 0, 0 };
        keyPad = new int[5][5];
        keyPad[0] = row0;
        keyPad[1] = row1;
        keyPad[2] = row2;
        keyPad[3] = row3;
        keyPad[4] = row4;

        currPlace = new int[]{2, 2};
    }
}
