package adventofcode2016.Day2Utils;

public class KeyPadDay2Part2 extends KeyPad {

    public KeyPadDay2Part2() {
        /*
              1
            2 3 4
          5 6 7 8 9
            A B C
              D
         */

        int[] row0 = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        int[] row1 = new int[] { 0, 0, 0, 1, 0, 0, 0 };
        int[] row2 = new int[] { 0, 0, 2, 3, 4, 0, 0 };
        int[] row3 = new int[] { 0, 5, 6, 7, 8, 9, 0 };
        int[] row4 = new int[] { 0, 0, 10, 11, 12, 0, 0 };
        int[] row5 = new int[] { 0, 0, 0, 13, 0, 0, 0 };
        int[] row6 = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        keyPad = new int[7][7];
        keyPad[0] = row0;
        keyPad[1] = row1;
        keyPad[2] = row2;
        keyPad[3] = row3;
        keyPad[4] = row4;
        keyPad[5] = row5;
        keyPad[6] = row6;

        currPlace = new int[]{3, 1};
    }
}
