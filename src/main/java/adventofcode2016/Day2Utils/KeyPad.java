package adventofcode2016.Day2Utils;

public abstract class KeyPad {

    int[][] keyPad;
    int[] currPlace;

    public String getCurrentNum() {
        int num = keyPad[currPlace[0]][currPlace[1]];
        if (num == 10) {
            return "A";
        }
        if (num == 11) {
            return "B";
        }
        if (num == 12) {
            return "C";
        }
        if (num == 13) {
            return "D";
        }
        return String.valueOf(num);
    }

    public void getNextStep(char c) {
        if (c == 'U') {
            updateCurrPlace(1, 0);
        }
        if (c == 'R') {
            updateCurrPlace(0, -1);
        }
        if (c == 'D') {
            updateCurrPlace(-1, 0);
        }
        if (c == 'L') {
            updateCurrPlace(0, 1);
        }
    }

    private void updateCurrPlace(int nextX, int nextY) {
        if (keyPad[currPlace[0] - nextX][currPlace[1] - nextY] != 0) {
            currPlace[0] -= nextX;
            currPlace[1] -= nextY;
        }
    }
}
