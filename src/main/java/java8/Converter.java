package java8;

@FunctionalInterface
interface Converter<F, T> {
    T convert(F from); // only one abstract method is allowed for a functional interface (i.e meant for lambda use)
    default void doSomething() {
        // do something in default method which is okay to define in interface
    }
}
