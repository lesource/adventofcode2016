package java8;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StringSorter {
    public static void main(String[] args) {
        classic();
        lamba();
        shortest();
        comparator();
        listSort();
    }

    private static void listSort() {
        System.out.println("listSort");
        List<String> names = Arrays.asList("peter", "anna", "mike", "xenia");
        System.out.println(names);
        names.sort(Comparator.reverseOrder());
        System.out.println(names);
    }

    private static void comparator() {
        System.out.println("comparator");
        List<String> names = Arrays.asList("peter", "anna", "mike", "xenia");
        System.out.println(names);
        Collections.sort(names, Comparator.reverseOrder());
        System.out.println(names);
    }

    private static void shortest() {
        System.out.println("shortest");
        List<String> names = Arrays.asList("peter", "anna", "mike", "xenia");
        System.out.println(names);
        Collections.sort(names, (a, b) -> b.compareTo(a));
        System.out.println(names);
    }

    private static void classic() {
        System.out.println("classic");
        List<String> names = Arrays.asList("peter", "anna", "mike", "xenia");
        System.out.println(names);

        Collections.sort(names, new Comparator<String>() {
            @Override
            public int compare(String a, String b) {
                return b.compareTo(a);
            }
        });
        System.out.println(names);
    }

    private static void lamba() {
        System.out.println("lambda");
        List<String> names = Arrays.asList("peter", "anna", "mike", "xenia");
        System.out.println(names);
        Collections.sort(names, (String a, String b) -> {
            return b.compareTo(a);
        });
        System.out.println(names);
    }
}
